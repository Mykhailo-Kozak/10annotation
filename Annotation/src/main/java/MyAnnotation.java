import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

public class MyAnnotation {
    public static void main(String[] args) {
        myMethod();
    }
    @Congratulation(name = "David", type = "Birthday")
    private static void myMethod(){
        System.out.println("Dear...");
    }
}

@Retention(RetentionPolicy.RUNTIME)
@Target(value = {ElementType.METHOD, ElementType.FIELD})
@interface Congratulation {
    String name();
    int age() default 45;
    String type();
}
